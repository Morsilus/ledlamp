package com.randiak_dev.ledlamp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.randiak_dev.ledlamp.ui.main.MainMenuAdapter;
import com.randiak_dev.ledlamp.visualizer.BarVisualizerView;
import com.randiak_dev.ledlamp.visualizer.VisualizerThread;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;

    FloatingActionButton fabBtMenu;

    public DataHandler dataHandler;

    public BluetoothHandler bluetoothHandler;
    public BarVisualizerView barVisualizerView;

    private BluetoothThread bluetoothThread;
    public VisualizerThread visualizerThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        }


        if (bluetoothHandler == null) {
            bluetoothHandler = new BluetoothHandler(this);
            bluetoothHandler.enableBT();
        }

        barVisualizerView = findViewById(R.id.barVisualizer);

        visualizerThread = new VisualizerThread(0);
        visualizerThread.setVisualizerEngineListener(new VisualizerThread.VisualizerEngineListener() {
            @Override
            public void onFftDataProcessed(float[] samples) {
                dataHandler.sendVisualizeData(samples);
            }
        });

        dataHandler = DataHandler.getInstance(bluetoothHandler, visualizerThread, this);
        dataHandler.start();

        tabLayout = findViewById(R.id.tlMainMenu);
        viewPager = findViewById(R.id.viewPager);

        for (int i = 0; i < 3; i++) {
            tabLayout.addTab(tabLayout.newTab());
        }
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        final MainMenuAdapter adapter = new MainMenuAdapter(this, getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

        fabBtMenu = findViewById(R.id.fabBtMenu);
    }

    public void showPairedDevices(View pView) {
        PopupMenu popupMenu = new PopupMenu(this, pView);
        MenuInflater inflater = popupMenu.getMenuInflater();
        this.bluetoothHandler.onPrepareOptionsMenu(popupMenu.getMenu());
        inflater.inflate(R.menu.bt_devices_menu, popupMenu.getMenu());
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
//                bluetoothThread = bluetoothHandler.connect(bluetoothHandler.getAddressOf(item.getItemId()));
                if (bluetoothHandler.connect(bluetoothHandler.getAddressOf(item.getItemId()))) {
                    fabBtMenu.setBackgroundTintList(ColorStateList.valueOf(0xff669900));
                    return true;
                } else {
                    fabBtMenu.setBackgroundTintList(ColorStateList.valueOf(0xffcc0000));
                    return false;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataHandler.interrupt();
        bluetoothHandler.disconnect();
    }
}
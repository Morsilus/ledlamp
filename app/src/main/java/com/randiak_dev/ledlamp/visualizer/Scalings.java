package com.randiak_dev.ledlamp.visualizer;

import android.util.Pair;
import java.util.LinkedList;

public class Scalings {
    public enum Scaling {LIN, LOG, MELv1, MELv2, MELv3, PITCH, OCT, OCT16, OCT32}
    public enum Direction {LtoR, RtoL, FromCenter, ToCenter}

    private static final double MIN_FREQ = 20;
    private static final double MAX_FREQ = 22050;
    private static final int[][] MEL_CONSTS = {{2592, 700}, {(int) (1000 / Math.log10(2)), 1000},
            {2410, 625}};

    public static LinkedList<Pair<Integer, Integer>> getDividers(Scaling scaling, Direction direction,
                                                                 int nDividers, int nFft,
                                                                 double minF, double maxF) {
        if (minF < MIN_FREQ) {
            minF = MIN_FREQ;
        }
        if (maxF > MAX_FREQ) {
            maxF = MAX_FREQ;
        }
        if (minF > maxF) {
            double buffer = minF;
            minF = maxF;
            maxF = buffer;
        }

        nDividers++;
        int[] dividers = new int[nDividers];
        switch (scaling) {
            case LIN:
                dividers = linScale(nDividers, nFft, minF, maxF);
                break;
            case LOG:
                dividers = logScale(nDividers, nFft, minF, maxF);
                break;
            case MELv1:
                dividers = melScale(nDividers, nFft, minF, maxF, 0);
                break;
            case MELv2:
                dividers = melScale(nDividers, nFft, minF, maxF, 1);
                break;
            case MELv3:
                dividers = melScale(nDividers, nFft, minF, maxF, 2);
                break;
            case PITCH:
                dividers = pitchScale(nDividers, nFft, minF, maxF);
                break;
            case OCT:
                if (nDividers == (8 ^ 1 + 1)) {
                    dividers = octaveScale(nFft, 1);
                }
                break;
            case OCT16:
                if (nDividers == (8 ^ 2 + 1)) {
                    dividers = octaveScale(nFft, 2);
                }
                break;
            case OCT32:
                if (nDividers == (8 ^ 3 + 1)) {
                    dividers = octaveScale(nFft, 3);
                }
                break;
        }

        LinkedList<Pair<Integer, Integer>> dividersList = new LinkedList<>();
        switch (direction) {
            case LtoR:
                for (int i = 0; i < (dividers.length - 1); i++) {
                    dividersList.add(new Pair<Integer, Integer>(dividers[i], dividers[i + 1]));
                }
                break;
            case RtoL:
                for (int i = (dividers.length - 1); i > 0; i--) {
                    dividersList.add(new Pair<Integer, Integer>(dividers[i], dividers[i - 1]));
                }
                break;
            case FromCenter:
                for (int i = 0; i < (dividers.length / 2); i++) {
                    int index = (dividers.length - 2) - 2 * i;
                    dividersList.add(new Pair<Integer, Integer>(dividers[index], dividers[index + 1]));
                }
                for (int i = (dividers.length / 2); i < (dividers.length - 1); i++) {
                    int index = 2 * (i - (dividers.length / 2));
                    dividersList.add(new Pair<Integer, Integer>(dividers[index], dividers[index + 1]));
                }
                break;
            case ToCenter:
//                for (int i = 0; i < dividers.length; i++) {
//                    int j = (dividers.length / 2) + ((int) Math.pow(-1, i + 1)) * ((i + 1) / 2) - 1;
//                    dividers[index][j] = dividers[dividers.length - 1 - i];
//                }
                break;
        }

        return dividersList;
    }

    private static int getFreqIndex(int nFft, double freq) {
        double freqUnit = (MAX_FREQ / (nFft - 1));
        return (int) (freq / freqUnit);
    }

    private static int[] linScale(int n, int nFft, double minF, double maxF) {
        int[] indexes = new int[n];
        double unit = (maxF - minF) / (n - 1);
        for (int i = 0; i < n; i++) {
            double freq = i * unit + minF;
            indexes[i] = getFreqIndex(nFft, freq);
        }
        return indexes;
    }

    private static int[] logScale(int n, int nFft, double minF, double maxF) {
        int[] indexes = new int[n];
        for (int i = 0; i < n; i++) {
            double freq = minF * Math.pow(maxF / minF, ((double) i) / (n - 1));
            indexes[i] = getFreqIndex(nFft, freq);
        }
        return indexes;
    }

    private static int[] pitchScale(int n, int nFft, double minF, double maxF) {
        int[] indexes = new int[n];
        double minP = hertzToPitch(minF);
        double maxP = hertzToPitch(maxF);
        double unit = (maxP - minP) / (n - 1);
        for (int i = 0; i < n; i++) {
            double freq = pitchToHertz(i * unit + minP);
            indexes[i] = getFreqIndex(nFft, freq);
        }
        return indexes;
    }

    private static double hertzToPitch(double freq) {
        return 12 * (Math.log(freq / 440) / Math.log(2)) + 69;
    }

    private static double pitchToHertz(double freq) {
        return 440 * Math.pow(2, (freq - 69) / 12);
    }

    private static int[] melScale(int n, int nFft, double minF, double maxF, int melVer) {
        int[] indexes = new int[n];
        double minM = hertzToMel(minF, melVer);
        double maxM = hertzToMel(maxF, melVer);
        double unit = (maxM - minM) / (n - 1);
        for (int i = 0; i < n; i++) {
            double freq = melToHertz(i * unit + minM, melVer);
            indexes[i] = getFreqIndex(nFft, freq);
        }
        return indexes;
    }

    private static double hertzToMel(double freq, int melVer) {
        return MEL_CONSTS[melVer][0] * Math.log10(1 + freq / MEL_CONSTS[melVer][1]);
    }

    private static double melToHertz(double freq, int melVer) {
        return MEL_CONSTS[melVer][1] * (Math.pow(10, freq / MEL_CONSTS[melVer][0]) - 1);
    }

    private static int[] octaveScale(int nFft, int octave) {
        int n = 8 ^ octave;
        int[] indexes = new int[n];
        for (int i = 0; i < n; i++) {
            double freq = 15.625 * Math.pow(2, (i + 1) / octave);
            indexes[i] = getFreqIndex(nFft, freq);
        }
        return indexes;
    }
}

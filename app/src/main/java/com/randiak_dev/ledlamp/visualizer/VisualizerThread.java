package com.randiak_dev.ledlamp.visualizer;

import android.media.audiofx.Visualizer;
import android.util.Pair;

import com.randiak_dev.ledlamp.BluetoothHandler;

import java.util.LinkedList;

public class VisualizerThread extends Thread implements Visualizer.OnDataCaptureListener {
    private Visualizer visualizer;
    int audioSession;
    private static final int CAPTURE_SIZE = 1024;
    Visualizer.MeasurementPeakRms mPeakRms;
    private static final double MIN_FREQ = 20;
    private static final double MAX_FREQ = 22050;

    private VisualizerEngineListener listener;

    private int bars = 16;
    private int spaces = 0;
    private int width = 1;

    private double[] weighting;
    private double[] prevs;
    private LinkedList<LinkedList<Pair<Integer, Integer>>> dividers;
    private int nFft;
    private double dbOffset = 0;
    private double powerRatio = 10; //15
    private double smoothing = 0.5;

    public VisualizerThread(int audioSession) {
        this.listener = null;
        this.audioSession = audioSession;

        nFft = CAPTURE_SIZE / 2 + 1;

        weighting = new double[nFft];
        this.setWeighting(Weightings.Weighting.A, 0.25);
//        weighting = Weightings.calcWeightings(Weightings.Weighting.A, nFft, MIN_FREQ, MAX_FREQ);
        prevs = new double[nFft];
        for (int i = 0; i < prevs.length; i++) {
            prevs[i] = 0;
        }
        dividers = new LinkedList<>();
        this.addScaling(Scalings.Scaling.LIN, Scalings.Direction.FromCenter, 20, 22050);
        this.addScaling(Scalings.Scaling.MELv1, Scalings.Direction.FromCenter, 600, 8000);
    }

    public void setBars(int bars) {
        this.bars = bars;
        this.setScaling(0, Scalings.Scaling.LIN, Scalings.Direction.FromCenter, 20, 22050);
        this.setScaling(1, Scalings.Scaling.MELv1, Scalings.Direction.FromCenter, 600, 8000);
    }

    public void setSpaces(int spaces) {
        this.spaces = spaces;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public synchronized void start() {
        if (visualizer == null) {
            visualizer = new Visualizer(audioSession);

            visualizer.setCaptureSize(CAPTURE_SIZE);
            visualizer.setScalingMode(Visualizer.SCALING_MODE_AS_PLAYED);
            visualizer.setMeasurementMode(Visualizer.MEASUREMENT_MODE_PEAK_RMS);
            visualizer.setDataCaptureListener(this, Visualizer.getMaxCaptureRate(), false, true);
            mPeakRms = new Visualizer.MeasurementPeakRms();
        }

        visualizer.setEnabled(true);
//        this.equalizer.setEnabled(true);
    }

    @Override
    public void interrupt() {
        visualizer.setEnabled(false);

        visualizer.release();
        visualizer = null;
//        this.equalizer.setEnabled(false);
    }

    public void setVisualizerEngineListener(VisualizerEngineListener listener) {
        this.listener = listener;
    }

    public void setScalingMode(int scalingMode) {
        this.visualizer.setEnabled(false);
        this.visualizer.setScalingMode(scalingMode);
        this.visualizer.setEnabled(true);
    }

    public void setWeighting(Weightings.Weighting weighting, double weightingRatio) {
        this.weighting = Weightings.calcWeightings(weighting, nFft, MIN_FREQ, MAX_FREQ);
        if (weightingRatio != 1) {
            for (int i = 0; i < this.weighting.length; i++) {
                this.weighting[i] *= weightingRatio;
            }
        }
    }

    public void addScaling(Scalings.Scaling scaling, Scalings.Direction direction,
                           double minF, double maxF) {
        dividers.add(Scalings.getDividers(scaling, direction, this.bars, this.nFft, minF, maxF));
    }

    public void setScaling(int index, Scalings.Scaling scaling, Scalings.Direction direction,
                           double minF, double maxF) {
        if (index < dividers.size()) {
            dividers.get(index).clear();
            dividers.set(index, Scalings.getDividers(scaling, direction, this.bars, this.nFft, minF, maxF));
        }
    }

    public void setDbOffset(double dbOffset) {
        this.dbOffset = dbOffset;
    }

    public void setPowerRatio(double powerRatio) {
        if (powerRatio > 1 && powerRatio < 100) {
            this.powerRatio = powerRatio;
        }
    }

    public void setSmoothing(double smoothing) {
        if (smoothing >=0 && smoothing <= 1) {
            this.smoothing = smoothing;
        }
    }

    private void processFft(byte[] fft) {
        float[] samples = new float[bars];
        for (LinkedList<Pair<Integer, Integer>> dividersList : dividers) {
            for (int bar = 0; bar < dividersList.size(); bar++) {
                for (int i = dividersList.get(bar).first; i <= dividersList.get(bar).second; i++) {
                    double amplitude = calcAmplitude(fft, i);
                    amplitude *= 256;
                    double dbs = calcDBs(amplitude);
                    dbs += weighting[i];
                    dbs += dbOffset;
                    double ratio = this.calcPowerRatio(dbs);
                    ratio = this.addSmoothing(ratio, i);
                    samples[bar] = (float) Math.max(samples[bar], ratio);
                }
            }
        }

        if (listener != null) {
            listener.onFftDataProcessed(samples);
        }
    }

    private double calcAmplitude(byte[] fft, int i) {
        if (i != 0 && i != (nFft - 1)) {
            return Math.hypot(fft[2 * i], fft[2 * i + 1]);
        } else if (i == 0) {
            return Math.abs(fft[0]) * 10;
        } else if (i == (nFft - 1)) {
            return Math.abs(fft[1]);
        }
        return 0;
    }

    private double calcDBs(double amplitude) {
        return 10 * Math.log10(amplitude) - 40;
    }

    private double calcPowerRatio(double db) {
        double ratio = Math.pow(10, db / this.powerRatio);
        if (ratio > 1) {
            ratio = 1;
        }
        return ratio;
    }

    private double addSmoothing(double ratio, int i) {
        ratio = this.smoothing * this.prevs[i] + (1 - this.smoothing) * ratio;
        this.prevs[i] = ratio;
        return ratio;
    }

    @Override
    public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
        processFft(fft);
    }

    @Override
    public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {}

    public interface VisualizerEngineListener {
        public void onFftDataProcessed(float[] samples);
    }
}

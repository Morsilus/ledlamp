package com.randiak_dev.ledlamp.ui.main;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.ColorUtils;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.randiak_dev.ledlamp.ColorPickerTable;
import com.randiak_dev.ledlamp.DataHandler;
import com.randiak_dev.ledlamp.R;
import com.randiak_dev.ledlamp.database.LampDatabase;
import com.randiak_dev.ledlamp.database.entities.Palette;
import com.rtugeek.android.colorseekbar.ColorSeekBar;

import java.util.List;

public class PalettesFragment extends Fragment {
    private LampDatabase database;
    private static View view;
    private static Context context;

    ColorSeekBar sbColor;
    Button btPickColor;

    Spinner spSpecialPalettes;

    Spinner spAddPalette;
    Button btSetPalette;
    LinearLayout llTablePaletteColors;
    ColorPickerTable colorPickerTable;

    private DataHandler dataHandler;

    public PalettesFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.palettes_fragment, container, false);
        context = getContext();
        database = LampDatabase.getInstance(context);
//        database = DatabaseCopier.getInstance(context).getRoomDatabase();
//        database.palettes().insertPalette(new com.randiak_dev.ledlamp.database.entities.Palette("nova", 0));

        sbColor = view.findViewById(R.id.sbPickColor);
        btPickColor = view.findViewById(R.id.btPickColor);

        spSpecialPalettes = view.findViewById(R.id.spSpecialPalettes);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, database.palettes().getPresetPalettesTitles());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSpecialPalettes.setAdapter(adapter);
        spSpecialPalettes.setBackground(database.palettes().getPalette(1).getGradientDrawable());

        spAddPalette = view.findViewById(R.id.spAddPalette);
        btSetPalette = view.findViewById(R.id.bSetPalette);
        llTablePaletteColors = view.findViewById(R.id.llTablePaletteColors);
        colorPickerTable = view.findViewById(R.id.tbPaletteColors);

        sbColor.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
            @Override
            public void onColorChangeListener(int colorBarPosition, int alphaBarPosition, int color) {
                btPickColor.setBackgroundColor(color);
                if ((color >> 3) > 0x3F || isBrightColor(color) || color == 0 || color == 255) {
                    btPickColor.setTextColor(0xFF000000);
                } else {
                    btPickColor.setTextColor(0xFFFFFFFF);
                }
            }
        });
        btPickColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onColorPicked(sbColor.getColor());
            }
        });

        spSpecialPalettes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onPresetPalettePicked(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        spAddPalette.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onCreateNewPalette();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        btSetPalette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSetPalette();
            }
        });

        this.dataHandler = DataHandler.getInstance();

        return view;
    }

//    private void onCreateCustomPalette() {
////        customPalettes.add(new Palette((ArrayList<Integer>)colorPickerTable.getColors()));
////        Palette palette = new Palette((ArrayList<Integer>)colorPickerTable.getColors());
////        btSetPalette.setBackground(palette.getGradientDrawable());
////        LampDatabase lampDatabase = LampDatabase.getInstance(context);
//        Gson gson = new Gson();
////        String str = gson.toJson(database.palettes().getPalette(1).getColors());
////        tv.setText(str);
//        Palette palette = new Palette("A", colorPickerTable.getColors());
////        btSetPalette.setBackground(palette.getGradientDrawable());
////
//        database.palettes().insertPalette(palette);
//        String str = gson.toJson(palette.getColors());
//        btSetPalette.setBackground(palette.getGradientDrawable());
//        tv.setText(str);
//    }

    private void onColorPicked(int color) {
        dataHandler.setColor(color);
    }

    private void onPresetPalettePicked(int index) {
        dataHandler.setSpecialPalette(index);
    }

    private void onCreateNewPalette() {
        colorPickerTable.createTable(Integer.parseInt((String)spAddPalette.getSelectedItem()));
    }

    private void onColorPicked() {

    }

    private void onSetPalette() {
        List<Integer> colorsList = colorPickerTable.getColors();
        int[] colors = new int[colorsList.size()];

        for (int i = 0; i < colorsList.size(); i++) {
            colors[i] = colorsList.get(i);
        }
        dataHandler.setCustomPalette(colors);
    }

    private static boolean isBrightColor(int color) {
        if (android.R.color.transparent == color) {
            return true;
        }
        int[] rgb = {Color.red(color), Color.green(color), Color.blue(color)};
        int brightness = (int) Math.sqrt((rgb[0] ^ 2) * 0.241 + (rgb[1] ^ 2) * 0.691 + (rgb[2] ^ 2) * 0.068);

        if (brightness >= 128) {
            return true;
        }
        return false;
    }
}

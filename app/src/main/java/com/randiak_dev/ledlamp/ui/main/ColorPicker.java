package com.randiak_dev.ledlamp.ui.main;

import android.content.Context;
import android.widget.Button;

public class ColorPicker extends Button {
    public ColorPicker(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(height, height);
    }
}

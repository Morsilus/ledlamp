package com.randiak_dev.ledlamp.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.randiak_dev.ledlamp.DataHandler;
import com.randiak_dev.ledlamp.R;
import com.randiak_dev.ledlamp.database.LampDatabase;
import com.randiak_dev.ledlamp.database.entities.Attribute;
import com.randiak_dev.ledlamp.database.entities.Effect;
import com.randiak_dev.r2itseekbar.R2ITSeekBar;

public class EffectsFragment extends Fragment {
    private View view;
    LampDatabase database;

    Button btColorPicker;
    Spinner spEffects;

    private LinearLayout llEffectProps;
    private R2ITSeekBar sbProps[];
    private Switch swProp4;

    private int index;
    private int parVals[] = {0, 0, 0, 0};

    private R2ITSeekBar sbSpeed;
    private R2ITSeekBar sbFadeBy;
    private R2ITSeekBar sbBrightness;
    private Switch swClockwise;

    private CheckBox cbGlitter;
    private R2ITSeekBar sbGlitter;
    private CheckBox cbStrobe;
    private R2ITSeekBar sbStrobe;

    private DataHandler dataHandler;
    
    public EffectsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.patterns_fragment, container, false);
        database = LampDatabase.getInstance(view.getContext());

        spEffects = view.findViewById(R.id.spEffects);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(),
                android.R.layout.simple_spinner_item, database.effects().getEffectsTitlesList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEffects.setAdapter(adapter);

        llEffectProps = view.findViewById(R.id.llEffectProps);

        sbProps = new R2ITSeekBar[3];
        sbProps[0] = view.findViewById(R.id.sbProp1);
        sbProps[1] = view.findViewById(R.id.sbProp2);
        sbProps[2] = view.findViewById(R.id.sbProp3);
        swProp4 = view.findViewById(R.id.swProp4);

        loadEffect(0);

        spEffects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loadEffect(position);
                processEffectParams();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                processEffectParams();
            }
        });

        for (int i = 0; i < sbProps.length; i++) {
            final int index = i;
            sbProps[i].setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
                @Override
                public void onProgressChanged(int progress) {
                    processEffectParams();
                    parVals[index] = (byte)(progress);
                }
            });
        }

        swProp4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                parVals[3] = isChecked ? 1 : 0;
                processEffectParams();
            }
        });

        sbSpeed = view.findViewById(R.id.sbSpeed);
        sbFadeBy = view.findViewById(R.id.sbFadeBy);
        sbBrightness = view.findViewById(R.id.sbBrightness);
        swClockwise = view.findViewById(R.id.swDirection);

        sbSpeed.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                speedChanged(progress);
            }
        });

        sbFadeBy.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                fadeByChanged(progress);
            }
        });

        sbBrightness.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                brightnessChanged(progress);
            }
        });

        swClockwise.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int val = isChecked ? 0 : 1;
                directionChanged(val);
            }
        });

        cbGlitter = view.findViewById(R.id.cbGlitter);
        sbGlitter = view.findViewById(R.id.sbGlitterRate);
        cbStrobe = view.findViewById(R.id.cbStrobe);
        sbStrobe = view.findViewById(R.id.sbStrobeRate);

        cbGlitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    glitterChanged(sbGlitter.getProgress());
                } else {
                    glitterChanged(0);
                }
            }
        });

        sbGlitter.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                if (cbGlitter.isChecked()) {
                    glitterChanged(progress);
                }
            }
        });

        cbStrobe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    strobeChanged(sbStrobe.getProgress());
                } else {
                    strobeChanged(0);
                }
            }
        });

        sbStrobe.setCustomSeekBarListener(new R2ITSeekBar.CustomSeekBarListener() {
            @Override
            public void onProgressChanged(int progress) {
                if (cbStrobe.isChecked()) {
                    strobeChanged(progress);
                }
            }
        });

        dataHandler = DataHandler.getInstance();

        return view;
    }

    private void loadEffect(int position) {
        this.index = position;

        llEffectProps.setVisibility(View.GONE);
        for (int i = 0; i < 3; i++) {
            sbProps[i].setVisibility(View.GONE);
        }
        swProp4.setVisibility(View.GONE);

        Effect actualEffect = database.effects().getEffect(position + 1);
        int index = 0;
        for (String attributeName : actualEffect.getAttributesNames()) {
            Attribute attribute = database.effects().getEffectAttributes(attributeName);
            if (index < 3 && attribute.getType() == 0) {
                sbProps[index].setTitle(attribute.getTitle());
                sbProps[index].setMin(attribute.getMinV());
                sbProps[index].setMax(attribute.getMaxV());
                sbProps[index].setProgress(attribute.getDefaultV());
                sbProps[index].setVisibility(View.VISIBLE);
                index++;
            } else if (attribute.getType() == 1) {
                swProp4.setText(attribute.getTitle());
                swProp4.setVisibility(View.VISIBLE);
            }
            llEffectProps.setVisibility(View.VISIBLE);
        }
    }

    private void speedChanged(int speed) {
        dataHandler.setSpeed(speed);
    }

    private void fadeByChanged(int fadeBy) {
        dataHandler.setFadeBy(fadeBy);
    }

    private void brightnessChanged(int brightness) {
        dataHandler.setBrightness(brightness);
    }

    private void directionChanged(int direction) {
        dataHandler.setDirection(direction);
    }

    private void glitterChanged(int glitter) {
        dataHandler.setGlitter(glitter);
    }

    private void strobeChanged(int strobe) {
        dataHandler.setStrobe(strobe);
    }

    private void processEffectParams() {
        dataHandler.setMode(index, parVals);
    }
}

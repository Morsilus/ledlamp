package com.randiak_dev.ledlamp.ui.main;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.randiak_dev.ledlamp.ui.main.Patterns.PatternFragment;

public class MainMenuAdapter extends FragmentPagerAdapter {
    private static String[] tabTitles = new String[]{"Palettes", "Patterns", "Visualizer"};
    private Context context;

    public MainMenuAdapter(Context pContext, FragmentManager pFM) {
        super(pFM);
        context = pContext;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PalettesFragment();
            case 1:
                return new EffectsFragment();
            case 2:
                return new VisualizerFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }
}

package com.randiak_dev.ledlamp.ui.main.Patterns;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class PatternsViewPager extends ViewPager {
    public PatternsViewPager(@NonNull Context context) {
        super(context);
    }

    public PatternsViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec, heightMeasureSpec);
            int h = child.getHeight();
            if (h > height)
                height = h;
        }
        if (height != 0)
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        View child = getChildAt(getCurrentItem());
        if (child != null) {
            child.measure(widthMeasureSpec, heightMeasureSpec);
//            super.onMeasure(widthMeasureSpec, child.getHeight());
        }
        setMeasuredDimension(getMeasuredWidth(), measureHeight(heightMeasureSpec, child));
    }

    private int measureHeight(int pMeasureSpec, View pView) {
        int result = 0;
        int specMode = MeasureSpec.getMode(pMeasureSpec);
        int specSize = MeasureSpec.getSize(pMeasureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            if (pView != null) {
                result = pView.getMeasuredHeight();
            }
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }
}

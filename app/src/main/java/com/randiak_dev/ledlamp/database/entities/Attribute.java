package com.randiak_dev.ledlamp.database.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Attribute {
    @PrimaryKey @NonNull
    private String name;
    @NonNull
    private String title;
    @NonNull
    private int type;
    private Integer minV;
    private Integer maxV;
    private Integer deltaV;
    private Integer defaultV;

    public Attribute(@NonNull String name, @NonNull String title, int type,
                     Integer minV, Integer maxV, Integer deltaV, Integer defaultV) {
        this.name = name;
        this.title = title;
        this.type = type;
        this.minV = minV;
        this.maxV = maxV;
        this.deltaV = deltaV;
        this.defaultV = defaultV;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public int getType() {
        return type;
    }

    public Integer getMinV() {
        return minV;
    }

    public Integer getMaxV() {
        return maxV;
    }

    public Integer getDeltaV() {
        return deltaV;
    }

    public Integer getDefaultV() {
        return defaultV;
    }
}

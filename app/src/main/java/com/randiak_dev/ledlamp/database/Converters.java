package com.randiak_dev.ledlamp.database;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.randiak_dev.ledlamp.database.entities.Attribute;

import java.lang.reflect.Type;
import java.util.List;

public class Converters {
    @TypeConverter
    public static String colorsToString(List<Integer> colors) {
        Gson gson = new Gson();
        return gson.toJson(colors);
    }
    @TypeConverter
    public List<Integer> stringToColors(String colorsString) {
        Type listType = new TypeToken<List<Integer>>() {}.getType();
        Gson gson = new Gson();
        return gson.fromJson(colorsString, listType);
    }

    @TypeConverter
    public static String attributesNamesToString(List<String> attributesNames) {
        Gson gson = new Gson();
        return gson.toJson(attributesNames);
    }
    @TypeConverter
    public List<String> stringToAttributesNames(String attributesNamesString) {
        Type listType = new TypeToken<List<String>>() {}.getType();
        Gson gson = new Gson();
        return gson.fromJson(attributesNamesString, listType);
    }
}

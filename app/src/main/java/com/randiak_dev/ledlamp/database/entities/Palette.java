package com.randiak_dev.ledlamp.database.entities;

import android.graphics.drawable.GradientDrawable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity
public class Palette {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String title;
    @NonNull
    private int preset;
    @NonNull
    private List<Integer> colors;
    @Ignore
    private GradientDrawable gradientDrawable;

    public Palette(@NonNull String title, @NonNull List<Integer> colors) {
        this.title = title;
        this.colors = colors;
        createGradientDrawable();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPreset(int preset) {
        this.preset = preset;
    }

    private void createGradientDrawable() {
        int gradientColors[] = new int[colors.size()];
        int i = 0;
        for (int gradientColor : colors) {
            gradientColors[i] = gradientColor;
            i++;
        }
        gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, gradientColors);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPreset() {
        return preset;
    }

    public List<Integer> getColors() {
        return colors;
    }

    public GradientDrawable getGradientDrawable() {
        return gradientDrawable;
    }
}
